/*
Copyright © 2023 Gustavo Silva <gustavo.silva@nunet.io>

*/
package main

import "example/nunet/cmd"

func main() {
	cmd.Execute()
}
